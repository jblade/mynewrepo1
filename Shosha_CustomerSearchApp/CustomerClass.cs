﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shosha_CustomerSearchApp
{

    class CustomerException : Exception
    {

    }

    class CustomerClass //This is the main class and represents the info displayed in the datagridview columns.
    {
        public int CustomerID { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public VapeClass CustomerDeviceName { get; set; } //Every Customer should have at least one Vape Device.
        public CoilClass CustomerCoils { get; set; } //Every Customer should have coils that they need to buy.
        public string ExtraNotes { get; set; } //We can save their favourite brands of e-juice and nicotine strength if necessary.

        static string myConnectionString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

        //SELECT data from the Database

        public CustomerClass()
        {
            // init your stuff here

            //BUT!!!! Raise an exception when something goes wrong!!
            //throw new CustomerException("dasda");
        }

        public DataTable Select()
        {
            //1. DB connection
            SqlConnection conn = new SqlConnection(myConnectionString);
            DataTable dt = new DataTable();
            try
            {
                //2. Write SQL Query
                string sql = "SELET * FROM CustomerTable";
                //Create cmd using sql and conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Create SQL DataAdapter using cmd
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                conn.Open();
                adapter.Fill(dt);

            }
            catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        //Insert Data into the Database
        public bool Insert(CustomerClass c)
        {
            //Create a default return rtpe and setting its value to false
            bool isSuccessful = false;

            //1. Connect the Database
            SqlConnection conn = new SqlConnection(myConnectionString);
            try
            {
                //2. Create a SQL Query to insert data
                string sql = "INSERT INTO CustomerTable (CustomerFirstName, CustomerLastName, CustomerDeviceName, CustomerCoils, ExtraNotes) VALUES (@CustomerFirstName, @CustomerLastName, @CustomerDeviceName, @CustomerCoils, @ExtraNotes)";
                //Create SQL Command using sql and conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //create parameters to add data
                cmd.Parameters.AddWithValue("@CustomerFirstName", c.CustomerFirstName);
                cmd.Parameters.AddWithValue("@CustomerLastName", c.CustomerLastName);
                cmd.Parameters.AddWithValue("@CustomerDeviceName", c.CustomerDeviceName);
                cmd.Parameters.AddWithValue("@CustomerCoils", c.CustomerCoils);
                cmd.Parameters.AddWithValue("@ExtraNotes", c.ExtraNotes);

                //connection open here
                conn.Open();
                int rows = cmd.ExecuteNonQuery();
                //If the query runs successfully then the value of rows will be greater than zero else its value will be 0
                if (rows > 0)
                {
                    isSuccessful = true;
                }
                else
                {
                    isSuccessful = false;
                }

            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                conn.Close();
            }
            return isSuccessful;

        }

        //Method to update data in database from the App
        public bool Update(CustomerClass c)
        {
            //create a default rerturn type and set its default value to false
            bool isSuccessful = false;
            SqlConnection conn = new SqlConnection(myConnectionString);
            try
            {
                //SQL to update data in our database
                string sql = "UPDATE CustomerTable SET CustomerFirstName=@CustomerFirstName, CustomerLastName=@CustomerLastName, CustomerDeviceName=@CustomerDeviceName, CustomerCoils=@CustomerCoils, ExtraNotes=@ExtraNotes WHERE CustomerID=@CustomerID";

                //create sql command
                SqlCommand cmd = new SqlCommand(sql, conn);
                //create parameters to add value
                cmd.Parameters.AddWithValue("@CustomerFirstName", c.CustomerFirstName);
                cmd.Parameters.AddWithValue("@CustomerLastName", c.CustomerLastName);
                cmd.Parameters.AddWithValue("@CustomerDeviceName", c.CustomerDeviceName);
                cmd.Parameters.AddWithValue("@CustomerCoils", c.CustomerCoils);
                cmd.Parameters.AddWithValue("@ExtraNotes", c.ExtraNotes);
                //Open Database connection
                conn.Open();

                int rows = cmd.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccessful = true;
                }
                else
                {
                    isSuccessful = false;
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return isSuccessful;            

        }
        //Method to DELETE data from the DB
        public bool Delete(CustomerClass c)
        {
            //create a default return value and set its value to false
            bool isSuccessful = false;
            SqlConnection conn = new SqlConnection(myConnectionString);
            try
            {
                //sql to delete data
                string sql = "DELETE FROM CustomerTable WHERE CustomerID=@CustomerID";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CustomerID", c.CustomerID);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccessful = true;
                }
                else
                {
                    isSuccessful = false;
                }
            }
            catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return isSuccessful;
        }

    }
}
