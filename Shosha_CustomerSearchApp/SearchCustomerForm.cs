﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;


namespace Shosha_CustomerSearchApp
{
    public partial class SearchCustomerForm : Form
    {
        public SearchCustomerForm()
        {
            InitializeComponent();
        }

        CustomerClass c = new CustomerClass(); //THIS IS SUPER IMPORTANT! Use this to access that class' properties!!!

        private void Button1_Click(object sender, EventArgs e) //Add NEW Customer!
        {
            //get the new value entered into the cell to be stored into the database


            string col1 = dgvCustomers[0, dgvCustomers.CurrentCell.RowIndex].Value.ToString();
            string col2 = dgvCustomers[1, dgvCustomers.CurrentCell.RowIndex].Value.ToString();

            c.CustomerFirstName = col1;
            c.CustomerLastName = col2;


        }

        private void SearchBtn_Click(object sender, EventArgs e) //This just does the same as the Search Function created below.
        {

        }
        static string myConnectionString = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;

        private void SearchTbx_TextChanged(object sender, EventArgs e) //This is where the Search functonality is created and executed first!
        {
            //get the value from the textbox
            string keyword = searchTbx.Text;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM CustomerTable WHERE CustomerFirstName LIKE '%" + keyword + "%' OR CustomerLastName LIKE '%" + keyword + "%'", conn);
        }

        private void button5_Click(object sender, EventArgs e) //BACK button goes to main menu!
        {
            
        }
    }
}
