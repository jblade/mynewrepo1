﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shosha_CustomerSearchApp
{
    class CoilClass
    {
        public int CoilID { get; set; }
        public string CoilName { get; set; }
        public int CoilPriceSingle { get; set; }
        public int CoilPrice3pk { get; set; }
        public int CoilPrice5pk { get; set; }
        public int NumberOfCores { get; set; } //single, double, triple etc.


    }
}
