﻿namespace Shosha_CustomerSearchApp
{
    partial class ShoshaCDS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginCodeTbx = new System.Windows.Forms.TextBox();
            this.loginCodeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(141, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(571, 42);
            this.label1.TabIndex = 2;
            this.label1.Text = "Shosha Customer Device Search";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(285, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(291, 39);
            this.label2.TabIndex = 3;
            this.label2.Text = "Enter Login Code:";
            // 
            // LoginCodeTbx
            // 
            this.LoginCodeTbx.Location = new System.Drawing.Point(356, 259);
            this.LoginCodeTbx.Name = "LoginCodeTbx";
            this.LoginCodeTbx.PasswordChar = '*';
            this.LoginCodeTbx.Size = new System.Drawing.Size(135, 22);
            this.LoginCodeTbx.TabIndex = 4;
            this.LoginCodeTbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginCodeTbx_KeyDown);
            // 
            // loginCodeBtn
            // 
            this.loginCodeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginCodeBtn.Location = new System.Drawing.Point(338, 312);
            this.loginCodeBtn.Name = "loginCodeBtn";
            this.loginCodeBtn.Size = new System.Drawing.Size(179, 74);
            this.loginCodeBtn.TabIndex = 5;
            this.loginCodeBtn.Text = "LOGIN";
            this.loginCodeBtn.UseVisualStyleBackColor = true;
            this.loginCodeBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // ShoshaCDS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 703);
            this.Controls.Add(this.loginCodeBtn);
            this.Controls.Add(this.LoginCodeTbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ShoshaCDS";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LoginCodeTbx;
        private System.Windows.Forms.Button loginCodeBtn;
    }
}

