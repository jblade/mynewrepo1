﻿namespace Shosha_CustomerSearchApp
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchCustomerBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NewDeviceBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SearchCustomerBtn
            // 
            this.SearchCustomerBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.SearchCustomerBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchCustomerBtn.Location = new System.Drawing.Point(330, 321);
            this.SearchCustomerBtn.Name = "SearchCustomerBtn";
            this.SearchCustomerBtn.Size = new System.Drawing.Size(225, 87);
            this.SearchCustomerBtn.TabIndex = 6;
            this.SearchCustomerBtn.Text = "Search Customer";
            this.SearchCustomerBtn.UseVisualStyleBackColor = false;
            this.SearchCustomerBtn.Click += new System.EventHandler(this.SearchCustomerBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(150, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(571, 42);
            this.label1.TabIndex = 5;
            this.label1.Text = "Shosha Customer Device Search";
            // 
            // NewDeviceBtn
            // 
            this.NewDeviceBtn.BackColor = System.Drawing.Color.Lime;
            this.NewDeviceBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewDeviceBtn.Location = new System.Drawing.Point(330, 226);
            this.NewDeviceBtn.Name = "NewDeviceBtn";
            this.NewDeviceBtn.Size = new System.Drawing.Size(225, 89);
            this.NewDeviceBtn.TabIndex = 4;
            this.NewDeviceBtn.Text = "New Device";
            this.NewDeviceBtn.UseVisualStyleBackColor = false;
            this.NewDeviceBtn.Click += new System.EventHandler(this.NewDeviceBtn_Click);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 703);
            this.Controls.Add(this.SearchCustomerBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NewDeviceBtn);
            this.Name = "MainMenuForm";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.MainMenuForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SearchCustomerBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button NewDeviceBtn;
    }
}