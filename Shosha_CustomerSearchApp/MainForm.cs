﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Shosha_CustomerSearchApp
{
    public partial class MainForm : Form
    {

        const string CONNSTRING_SETTING = "myConnectionString";
        string myConnectionString;
        public Boolean InitializedOK { get; set; }


        public MainForm()
        {
            InitializeComponent();
            InitializedOK = false;

            try
            {
                myConnectionString = ConfigurationManager.ConnectionStrings[CONNSTRING_SETTING].ConnectionString;

                CustomerClass c = new CustomerClass(); //THIS IS SUPER IMPORTANT! Use this to access that class' properties!!!



                InitializedOK = true;
            }
            catch (Exception)
            {
                //Shhh... don't tell anyone!!
            }

        }


        private void SearchTbx_TextChanged(object sender, EventArgs e)
        {
            //get the value from the textbox
            string keyword = searchTbx.Text;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM CustomerTable WHERE CustomerFirstName LIKE '%" + keyword + "%' OR CustomerLastName LIKE '%" + keyword + "%'", conn);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //get the new value entered into the cell to be stored into the database
            

            string col1 = dgvCustomers[0, dgvCustomers.CurrentCell.RowIndex].Value.ToString();
            string col2 = dgvCustomers[1, dgvCustomers.CurrentCell.RowIndex].Value.ToString();

            c.CustomerFirstName = col1;
            c.CustomerLastName = col2;
            
        }

        
    }
}
